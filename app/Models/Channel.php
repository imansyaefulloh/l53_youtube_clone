<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use Searchable;

    protected $table = 'channels';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'image_filename',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * RELATIONSHIPS
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function getImage()
    {
        if (!$this->image_filename) {
            return config('youtube.buckets.images') . '/profile/default_avatar.png';
        }

        return config('youtube.buckets.images') . '/profile/' . $this->image_filename;
    }

    public function subscriptionCount()
    {
        return $this->subscriptions->count();
    }

    public function totalVideoViews()
    {
        return $this->hasManyThrough(VideoView::class, Video::class)->count();
    }
}
















