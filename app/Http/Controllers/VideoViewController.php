<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Video;
use App\Models\VideoView;
use Illuminate\Http\Request;

class VideoViewController extends Controller
{
    const BUFFER = 5;

    public function create(Request $request, Video $video)
    {
        if (!$video->canBeAccessed($request->user())) {
            return;
        }

        if ($request->user()) {
            $lastUserView = $video->views()->latestByUser($request->user())->first();

            if ($lastUserView != null) {
                if ($this->withinBuffer($lastUserView)) {
                    return;
                }    
            }
        }

        $lastIpView = $video->views()->latestByIp($request->ip())->first();

        if ($lastIpView != null) {
            if ($this->withinBuffer($lastIpView)) {
                return;
            }
        }

        $video->views()->create([
            'user_id' => $request->user() ? $request->user()->id : null,
            'ip' => $request->ip()
        ]);

        return response()->json(null, 200);
    }

    public function withinBuffer(VideoView $view)
    {
        return $view && $view->created_at->diffInSeconds(Carbon::now()) < self::BUFFER;
    }
}


























