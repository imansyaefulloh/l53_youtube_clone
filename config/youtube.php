<?php

return [
    'buckets' => [
        // 'videos' => 'https://s3.amazonaws.com/videos.imansyaefulloh.com',
        // 'images' => 'https://s3.amazonaws.com/images.imansyaefulloh.com',
        'videos' => 'https://s3-ap-southeast-1.amazonaws.com/videos.imansyaefulloh.com',
        'images' => 'https://s3-ap-southeast-1.amazonaws.com/images.imansyaefulloh.com',
    ]
];